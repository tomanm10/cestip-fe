package cz.cestip.webApp.model;

import java.util.Calendar;

public class EvolutionPrice {
    private Integer price;
    private Calendar date;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
