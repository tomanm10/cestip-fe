package cz.cestip.webApp.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Boarding {
    FULL_BOARD("Plná penze"),
    ALL_INCLUSIVE("All inclusive"),
    HALF_BOARD("Polopenze"),
    NONE("Bez stravy");

    private final String descriptiveName;

    private Boarding(String descriptiveName){
        this.descriptiveName = descriptiveName;
    }

    public String getDescriptiveName() {
        return descriptiveName;
    }

    public String getName(){
        return this.name();
    }
}
