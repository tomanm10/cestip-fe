package cz.cestip.webApp.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AccommodationType {
    APARTMENT("Apartmán"),
    HOTEL("Hotel"),
    BUNGALOW("Bungalov");

    private final String descriptiveName;

    private AccommodationType(String descriptiveName) {
        this.descriptiveName = descriptiveName;
    }

    public String getDescriptiveName() {
        return descriptiveName;
    }

    public String getName(){
        return this.name();
    }
}
