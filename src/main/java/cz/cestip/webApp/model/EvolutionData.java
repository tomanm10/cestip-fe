package cz.cestip.webApp.model;

import java.util.List;

public class EvolutionData {
    private String travelAgency;
    private List<EvolutionPrice> prices;

    public String getTravelAgency() {
        return travelAgency;
    }

    public void setTravelAgency(String travelAgency) {
        this.travelAgency = travelAgency;
    }

    public List<EvolutionPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<EvolutionPrice> prices) {
        this.prices = prices;
    }
}
