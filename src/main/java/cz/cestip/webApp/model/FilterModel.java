package cz.cestip.webApp.model;

import java.util.List;

public class FilterModel {
    private Integer minPrice;
    private Integer maxPrice;
    private Boarding[] boardings;
    private AccommodationType[] accommodationTypes;
    private List<Destination> destinations;
    private Room[] rooms;

    public FilterModel(Integer minPrice, Integer maxPrice, Boarding[] boardings, AccommodationType[] accommodationTypes, List<Destination> destinations, Room[] rooms) {
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.boardings = boardings;
        this.accommodationTypes = accommodationTypes;
        this.destinations = destinations;
        this.rooms = rooms;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Boarding[] getBoardings() {
        return boardings;
    }

    public void setBoardings(Boarding[] boardings) {
        this.boardings = boardings;
    }

    public AccommodationType[] getAccommodationTypes() {
        return accommodationTypes;
    }

    public void setAccommodationTypes(AccommodationType[] accommodationTypes) {
        this.accommodationTypes = accommodationTypes;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }

    public Room[] getRooms() {
        return rooms;
    }

    public void setRooms(Room[] rooms) {
        this.rooms = rooms;
    }
}
