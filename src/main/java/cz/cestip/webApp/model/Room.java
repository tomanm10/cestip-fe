package cz.cestip.webApp.model;

public class Room {
    private Integer adults;
    private Integer children;

    public Room(Integer adults, Integer children) {
        this.adults = adults;
        this.children = children;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }
}
