/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cestip.webApp.service.security;

import cz.cestip.webApp.model.User;
import cz.cestip.webApp.rest.clients.UserServiceClient;
import cz.cestip.webApp.security.SecurityUtils;
import cz.cestip.webApp.security.model.OAuth2UserInfo;
import cz.cestip.webApp.security.model.OAuth2UserInfoFactory;
import cz.cestip.webApp.security.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

@Service
public class OAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    UserServiceClient userServiceClient;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());

        User user;
        try {
            user = userServiceClient.getByMail(oAuth2UserInfo.getEmail());
        }catch(Exception e){ //automatic registration of user
            user = new User();
            user.setEmail(oAuth2UserInfo.getEmail());

            userServiceClient.registerUser(user);
        }
        
        UserDetails details = new UserDetails(user, oAuth2User.getAttributes());
        SecurityUtils.setCurrentUser(details);
        return details;
    }
}