package cz.cestip.webApp.rest;

import cz.cestip.webApp.model.TravelAgency;
import cz.cestip.webApp.rest.clients.DataServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * Endpoint that acts as proxy to data service's travel agencies endpoint
 */
@RestController
@RequestMapping("/travelagencies")
public class TravelAgencyController {

    @Autowired
    DataServiceClient dataClient;

    @GetMapping
    public List<TravelAgency> getTravelAgencies(){
        return dataClient.getTravelAgencies();
    }
}