package cz.cestip.webApp.rest;

import cz.cestip.webApp.model.AccommodationType;
import cz.cestip.webApp.model.Boarding;
import cz.cestip.webApp.model.EvolutionData;
import cz.cestip.webApp.rest.clients.DataServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;

/**
 * Endpoint that acts as proxy to data service's statistics endpoint
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    DataServiceClient dataClient;

    @GetMapping(value="/prices/avg/evolution")
    public List<EvolutionData> getEvolutionAveragePrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getEvolutionAveragePrices(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }

    @GetMapping(value="/prices/min/evolution")
    public List<EvolutionData> getEvolutionMinPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getEvolutionMinPrices(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }

    @GetMapping(value="/prices/max/evolution")
    public List<EvolutionData> getEvolutionMaxPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getEvolutionMaxPrices(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }

    @GetMapping(value="/prices/avg")
    public Object getAveragePrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getAveragePrices(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }

    @GetMapping(value="/prices/max")
    public Object getMaxPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getMaxPrices(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }

    @GetMapping(value="/prices/min")
    public Object getMinPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getMinPrices(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }

    @GetMapping(value="/term-count")
    public Object getTermCounts(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        return dataClient.getTermCounts(from,
                to,
                destination,
                accommodationType,
                boarding,adults,
                kids,
                minPrice,
                maxPrice
        );
    }
}
