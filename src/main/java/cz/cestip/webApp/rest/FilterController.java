package cz.cestip.webApp.rest;

import cz.cestip.webApp.model.AccommodationType;
import cz.cestip.webApp.model.Boarding;
import cz.cestip.webApp.model.FilterModel;
import cz.cestip.webApp.model.Room;
import cz.cestip.webApp.rest.clients.DataServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  API endpoint that returns possible filter values
 */
@RestController
@RequestMapping("/filters")
public class FilterController {

    @Autowired
    DataServiceClient dataClient;

    @GetMapping
    public Object getFilters(){
        int minPrice = 0;
        int maxPrice = 250000;
        Room[] rooms = new Room[]{
                new Room(2,0),
                new Room(2,1),
                new Room(2,2),
                new Room(2,4)
        };

        return new FilterModel(
                minPrice,
                maxPrice,
                Boarding.values(),
                AccommodationType.values(),
                dataClient.getDestinations(),
                rooms
        );
    }
}
