package cz.cestip.webApp.rest;

import cz.cestip.webApp.model.AccommodationType;
import cz.cestip.webApp.model.Boarding;
import cz.cestip.webApp.model.Destination;
import cz.cestip.webApp.model.Term;
import cz.cestip.webApp.rest.clients.DataServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.List;
/**
 * Endpoint that acts as proxy to data service's terms endpoint
 */
@RestController
@RequestMapping("/terms")
public class TermController {

    @Autowired
    DataServiceClient dataClient;

    /**
     * Get currently offered terms (downloaded today or yesterday)
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping
    public List<Term> getCurrentTerms(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    ){
        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar now = Calendar.getInstance();
        now.set(2020,Calendar.MAY,20);

        Calendar yesterday = Calendar.getInstance();
        yesterday.set(2020,Calendar.MAY,19);

        return dataClient.getTerms(
                from,
                to,
                destination,
                accommodationType,
                boarding,
                adults,
                kids,
                minPrice,
                maxPrice,
                yesterday,
                now
        );
    }
}
