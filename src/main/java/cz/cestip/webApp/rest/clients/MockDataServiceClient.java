package cz.cestip.webApp.rest.clients;

import cz.cestip.webApp.model.*;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component
@Primary
@Profile("dev")
public class MockDataServiceClient implements DataServiceClient {

    @Override
    public List<Term> getTerms(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice,
            Calendar downloadDateFrom,
            Calendar downloadDateTo
    ) {
        int count = (int) (Math.random()*100);
        List<Term> out = new ArrayList<>(count);
        for(int i=0;i<count;i++){
            Term t = generateTerm();

            out.add(t);
        }
        return out;
    }

    @Override
    public List<Destination> getDestinations() {
        int count = (int) (Math.random()*100);
        List<Destination> out = new ArrayList<>(count);
        for(int i=0;i<count;i++){
            Destination d = generateDestination();
            out.add(d);
        }
        return out;
    }

    @Override
    public List<TravelAgency> getTravelAgencies() {
        int count = 5;
        List<TravelAgency> out = new ArrayList<>(count);
        for(int i=0;i<count;i++){
            TravelAgency t = generateTravelAgency();
            out.add(t);
        }
        return out;
    }


    public Accommodation generateAccomodation(){
        int i = (int) (Math.random()*100000);
        Accommodation a = new Accommodation();
        a.setCity("Cool city of accomodation "+i);
        a.setClassification(i%5+1);
        a.setCountry("Cool country of accomodation "+i);
        a.setDescription("Decent description of accomodation "+i);
        a.setId(i);
        a.setName("Very cool accomodation "+i);
        a.setRating(i%5+1);
        a.setStreet("Not so cool street of accomodation "+i);
        a.setUrl("http://verycoolaccomodation"+i+".com");

        return a;
    }

    public TravelAgency generateTravelAgency(){
        int i = (int) (Math.random()*100000);
        TravelAgency t = new TravelAgency();
        t.setDescription("Descriptive description of travel agency "+i);
        t.setId(i);
        t.setLogoUrl("https://dsc.invia.cz/mkt/press/new-logo.png");
        t.setName("Ultra mega super travel agency "+i);
        t.setRating(i%5+1);
        t.setUrl("http://generictravelagency"+i+".com");

        return t;
    }

    public Term generateTerm(){
        int i = (int) (Math.random()*100000);
        Term t = new Term();
        t.setId(i);

        t.setAccommodationType(
                i%3==0
                        ? AccommodationType.APARTMENT
                        : i%7==0
                        ? AccommodationType.BUNGALOW
                        : AccommodationType.HOTEL
        );
        t.setBoarding(
                i%4==0
                        ? Boarding.HALF_BOARD
                        : i%17==0
                        ? Boarding.NONE
                        : i%9==0
                        ? Boarding.ALL_INCLUSIVE
                        : Boarding.FULL_BOARD
        );

        Trip trip = generateTrip();
        t.setTrip(trip);

        t.setAdults(i%3);
        t.setKids(i%2);

        Calendar fromMock = Calendar.getInstance();
        fromMock.setTimeInMillis(Math.round(Math.random()*1586206297420L));
        t.setFrom(fromMock);

        Calendar toMock = Calendar.getInstance();
        toMock.setTimeInMillis((i%20*86400000)+fromMock.getTimeInMillis());
        t.setTo(toMock);

        t.setOriginalPrice(i%20*1000);
        t.setPrice((int) (t.getOriginalPrice()*Math.random()));
        return t;
    }

    public Trip generateTrip(){
        int i = (int) (Math.random()*10000);
        Trip trip = new Trip();
        trip.setAccommodation(generateAccomodation());
        trip.setDescription("Very cool description of trip "+i);
        trip.setDestination(generateDestination());
        trip.setId(i);
        trip.setTravelAgency(generateTravelAgency());
        trip.setUrl(trip.getTravelAgency().getUrl()+"/trips/"+i);
        return trip;
    }

    public Destination generateDestination(){
        int i = (int) (Math.random()*10000);
        Destination d = new Destination();
        d.setId(i);
        d.setName("Random location "+i);
        d.setDescription("Very descriptive description of random location "+i);
        d.setLat((Math.random()*180)-90);
        d.setLng((Math.random()*360)-180);
        return d;
    }

    @Override
    public List<EvolutionData> getEvolutionAveragePrices(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        int max = 5;
        List<EvolutionData> out = new ArrayList<>(max);
        for(int i=0;i<max;i++){
            out.add(generateEvolutionData());
        }
        return out;
    }

    @Override
    public List<EvolutionData> getEvolutionMaxPrices(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        return getEvolutionAveragePrices(from,to,destination,accommodationType,boarding,adults,kids,minPrice,maxPrice);
    }

    @Override
    public List<EvolutionData> getEvolutionMinPrices(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        return getEvolutionAveragePrices(from,to,destination,accommodationType,boarding,adults,kids,minPrice,maxPrice);
    }

    @Override
    public List<PriceData> getAveragePrices(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        int max = 5;
        List<PriceData> out = new ArrayList<>(max);
        for(int i=0;i<max;i++){
            out.add(generatePriceData());
        }
        return out;
    }

    @Override
    public List<PriceData> getMaxPrices(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        return getAveragePrices(from,to,destination,accommodationType,boarding,adults,kids,minPrice,maxPrice);
    }

    @Override
    public List<PriceData> getMinPrices(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        return getAveragePrices(from,to,destination,accommodationType,boarding,adults,kids,minPrice,maxPrice);
    }

    @Override
    public List<TermCount> getTermCounts(
            Calendar from,
            Calendar to,
            String destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice
    ) {
        int max = 5;
        List<TermCount> out = new ArrayList<>(max);
        for(int i=0;i<max;i++){
            out.add(generateTermCount());
        }
        return out;
    }

    private TermCount generateTermCount(){
        TermCount out = new TermCount();
        out.setCount((int) (Math.random()*1000));
        out.setTravelAgency("Travel agency "+(int) (Math.random()*1000));
        return out;
    }

    private PriceData generatePriceData(){
        PriceData out = new PriceData();
        out.setPrice((int) (Math.random()*100000));
        out.setTravelAgency("Travel agency "+(int) (Math.random()*1000));
        return out;
    }

    private EvolutionData generateEvolutionData(){
        int i = (int) (Math.random()*1000);
        EvolutionData out = new EvolutionData();
        out.setTravelAgency("Travel agency "+i);
        out.setPrices(generateEvolutionPrices());

        return out;
    }

    private List<EvolutionPrice> generateEvolutionPrices(){
        List<EvolutionPrice> out = new ArrayList<>(7);
        for(int i=0;i<7;i++){
            EvolutionPrice price = new EvolutionPrice();
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis()-i*86400001);
            price.setDate(c);
            price.setPrice((int) (Math.random()*100000));

            out.add(price);
        }

        return out;
    }
}
