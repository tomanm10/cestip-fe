package cz.cestip.webApp.rest.clients;

import cz.cestip.webApp.model.User;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Primary
@Profile("dev")
public class MockUserServiceClient implements UserServiceClient{
    @Override
    public ResponseEntity<Void> registerUser(User user) {
        return null;
    }

    @Override
    public User getByMail(String email) {
        var out = new User();
        out.setEmail(email);
        return out;
    }
}
