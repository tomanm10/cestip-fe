package cz.cestip.webApp.rest.clients;

import cz.cestip.webApp.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "userService", primary = false)
public interface UserServiceClient {

    @RequestMapping(method = RequestMethod.GET, value="/users")
    public ResponseEntity<Void> registerUser(@RequestBody User user);

    @RequestMapping(method = RequestMethod.GET, value="/users/{email}")
    public User getByMail(@PathVariable String email);
}

