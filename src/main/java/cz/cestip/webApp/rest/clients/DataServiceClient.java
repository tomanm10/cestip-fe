package cz.cestip.webApp.rest.clients;

import cz.cestip.webApp.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;
import java.util.List;

@FeignClient(name = "dataService", primary = false)
public interface DataServiceClient {

    @RequestMapping(method = RequestMethod.GET, value = "/terms")
    List<Term> getTerms(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar downloadDateFrom,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar downloadDateTo
    );

    @RequestMapping(method = RequestMethod.GET, value = "/destinations")
    List<Destination> getDestinations();

    @RequestMapping(method = RequestMethod.GET, value = "/travelagencies")
    List<TravelAgency> getTravelAgencies();

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/prices/avg/evolution")
    public List<EvolutionData> getEvolutionAveragePrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/prices/max/evolution")
    public List<EvolutionData> getEvolutionMaxPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/prices/min/evolution")
    public List<EvolutionData> getEvolutionMinPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/prices/avg")
    public List<PriceData> getAveragePrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/prices/max")
    public List<PriceData> getMaxPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/prices/min")
    public List<PriceData> getMinPrices(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

    @RequestMapping(method = RequestMethod.GET, value = "/statistics/term-count")
    public List<TermCount> getTermCounts(
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required=false) String destination,
            @RequestParam(required=false) AccommodationType accommodationType,
            @RequestParam(required=false) Boarding boarding,
            @RequestParam(required=false) Integer adults,
            @RequestParam(required=false) Integer kids,
            @RequestParam(required=false) Integer minPrice,
            @RequestParam(required=false) Integer maxPrice
    );

}
