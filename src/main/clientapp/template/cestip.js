// all should be within ready event because this script is imported by React before document is loaded
document.addEventListener("ready", function () {

    document.getElementById("open-filters-icon").addEventListener("click", function () {
        // TODO animation
        document.getElementById("filters").classList.add("d-block");
        document.getElementById("close-filters-icon").classList.remove("d-none");
        this.classList.add("d-none");
    });

    document.getElementById("close-filters-icon").addEventListener("click", function () {
        // TODO animation
        document.getElementById("filters").classList.remove("d-block");
        document.getElementById("open-filters-icon").classList.remove("d-none");
        this.classList.add("d-none");
    });

});