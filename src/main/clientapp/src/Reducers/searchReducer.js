export const searchReducer = (state, action) => {
    switch (action.type) {

        case 'SET_DESTINATION':
            if (action.destination === state.destination) {
                return state;
            }
            return {
                ...state,
                destination: action.destination

            };

        case 'SET_FOOD':
            return {
                ...state,
                food: action.food
            };

        case 'SET_PRIZE':
            return {
                ...state,
                prize: action.prize
            };

        case 'SET_TRAVELERS':
            return {
                ...state,
                travelers: action.travelers
            };

        case 'SET_DATE':
            return {
                ...state,
                date: action.date
            };

        case 'SET_TRAVEL_TYPE':
            return {
                ...state,
                travelType: action.travelType
            };

        default:
            console.error("unknown type");
            return state
    }
};

