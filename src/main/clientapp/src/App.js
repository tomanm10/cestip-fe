import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './static/cestip.css';
import 'jquery/dist/jquery.min.js';
import 'popper.js/dist/popper.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'rc-slider/assets/index.css';

import Navbar from "./Components/Navbar";
import FilterTab from "./Components/FilterTab";
import TravelCompanies from "./Components/TravelCompanies";
import Map from "./Components/Map";
import DefaultFooter from "./Components/DefaultFooter";
import ResultSection from "./Components/ResultSection";
import SearchContextProvider from "./Contexts/SearchContext";
import CurrencySettings from "./Components/CurrencySettings";
import About from "./Components/About";
import FilterApiContextProvider from "./Contexts/FilterApiContext";
import TermApiContextProvider from "./Contexts/TermApiContext";
import CurrencyContextProvider from "./Contexts/CurrencyContext";

function App() {
    return (
        <div>
            <Navbar/>
            <main>
                <CurrencyContextProvider>
                    <FilterApiContextProvider>
                        <SearchContextProvider>
                            <TermApiContextProvider>
                                <FilterTab/>
                                <ResultSection/>
                                <CurrencySettings/>
                                <TravelCompanies/>
                                <Map/>
                            </TermApiContextProvider>
                        </SearchContextProvider>
                    </FilterApiContextProvider>
                </CurrencyContextProvider>
                <About/>
                <DefaultFooter/>
            </main>
        </div>
    );
}

export default App;
