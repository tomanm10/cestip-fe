import {useEffect, useState} from "react";

/**
 * @param {string} initialUrl REST API endpoint URL; MUST BE WITHOUT FIRST /
 * @param {Object} defaultData default data used before fetch returns data from given endpoint
 */
const useApiService = (initialUrl, defaultData) => {
    const [data, setData] = useState(defaultData);
    const [url, setUrl] = useState(initialUrl);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            setIsError(false);
            setIsLoading(true);
            try {
                const result = await fetch(process.env.REACT_APP_ENDPOINTS_DOMAIN + url)
                    .then(response => response.json());
                setData(result);
            } catch (error) {
                setIsError(true);
            }
            setIsLoading(false);
        };
        fetchData();
    }, [url]);

    return [data, isLoading, isError, setUrl];
};

export default useApiService;