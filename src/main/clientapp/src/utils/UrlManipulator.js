class UrlManipulator {

    static ARRAY_ITEM_SEPARATOR = ",";

    /**
     * Writes JSON object with filters to URL querystring.
     * @param filters
     * @return {string}
     */
    static writeFilters(filters) {
        const url = new URLSearchParams();

        for (let [key, value] of Object.entries(filters)) {

            if (value instanceof Array) {
                value = value.reduce((previousValue, currentValue) => {
                    if (previousValue === null || currentValue === null) {
                        return '';
                    }
                    if (previousValue instanceof Date) {
                        previousValue = UrlManipulator.formatDateEn(previousValue);
                    }
                    if (currentValue instanceof Date) {
                        currentValue = UrlManipulator.formatDateEn(currentValue);
                    }
                    return `${previousValue}${UrlManipulator.ARRAY_ITEM_SEPARATOR}${currentValue}`
                });
            }

            if (value === null || value === undefined || value === '') {
                continue;
            }
            url.append(key, value.toString());
        }

        return url.toString();
    }

    /**
     * Reads filters from current URL and returns JSON object from them.
     * @return {{
     *    date: Date[],
     *    travelType: string,
     *    destination: string,
     *    prize: number[],
     *    food: string,
     *    travelers: string
     * }}
     */
    static readFilters() {
        const url = new URLSearchParams(window.location.search);
        // defaults
        let filters = {
            destination: "",
            prize: [undefined, undefined], // array of min and max prize
            food: "",
            date: [null, null], // array of from and to dates (vanilla JS Date objects)
            travelType: "",
            travelers: ""
        }

        url.forEach((value, key) => {
            value = value.split(UrlManipulator.ARRAY_ITEM_SEPARATOR);
            if (key === "date") {
                value = value.map(item => new Date(item));
            }
            filters[key] = (value.length === 1) ? value[0] : value;
        });

        return filters;
    }

    /**
     * Formats given date to YYYY-mm-dd.
     * @param date
     * @return {string}
     */
    static formatDateEn(date) {
        const y = new Intl.DateTimeFormat('en', {year: 'numeric'}).format(date);
        const m = new Intl.DateTimeFormat('en', {month: '2-digit'}).format(date);
        const d = new Intl.DateTimeFormat('en', {day: '2-digit'}).format(date);
        return `${y}-${m}-${d}`;
    }

    /**
     * Writes JSON object with filters to URL querystring used when calling REST API.
     * @param filters
     * @return {string}
     */
    static writeFiltersForApi(filters) {
        let filtersApi = {};

        if (filters.date[0] !== null) {
            filtersApi.from = UrlManipulator.formatDateEn(filters.date[0]);
        }
        if (filters.date[1] !== null) {
            filtersApi.to = UrlManipulator.formatDateEn(filters.date[1]);
        }
        if (filters.destination !== "") {
            filtersApi.destination = filters.destination;
        }
        if (filters.travelType !== "") {
            filtersApi.accommodationType = filters.travelType;
        }
        if (filters.food !== "") {
            filtersApi.boarding = filters.food;
        }
        if (filters.travelers !== "") {
            const people = filters.travelers.split(" + ");
            filtersApi.adults = people[0];
            filtersApi.kids = people[1];
        }
        if (filters.prize[0] !== undefined) {
            filtersApi.minPrice = Math.round(filters.prize[0]);
        }
        if (filters.prize[1] !== undefined) {
            filtersApi.maxPrice = Math.round(filters.prize[1]);
        }

        return (new URLSearchParams(filtersApi)).toString();
    }
}

export default UrlManipulator;