import React, {createContext, useEffect} from 'react';
import useApiService from "../utils/ApiService";

export const FilterApiContext = createContext();

const FilterApiContextProvider = (props) => {
    const defaultData = {
        minPrice: 0,
        maxPrice: 250000,
        boardings: [],
        accommodationTypes: [],
        destinations: [],
        rooms: []
    };

    const [data, isLoading, isError] = useApiService('filters', defaultData);

    useEffect(() => {
    }, [data, isLoading, isError]);


    return (
        <FilterApiContext.Provider value={{data}}>
            {props.children}
        </FilterApiContext.Provider>
    );
};


export default FilterApiContextProvider;



