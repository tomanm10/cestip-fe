import React, {createContext, useEffect, useReducer, useRef} from 'react';
import {searchReducer} from "../Reducers/searchReducer";
import {useHistory} from 'react-router-dom';
import UrlManipulator from "../utils/UrlManipulator";

export const SearchContext = createContext();

const SearchContextProvider = (props) => {
    const history = useHistory();
    let [filters, dispatch] = useReducer(searchReducer, UrlManipulator.readFilters());

    const componentIsMounted = useRef(false);
    useEffect(() => {
        if (componentIsMounted.current) {
            history.push({
                pathname: '',
                search: UrlManipulator.writeFilters(filters)
            });
        } else {
            componentIsMounted.current = true;
        }

    }, [filters, history]);


    return (
        <SearchContext.Provider value={{filters, dispatch}}>
            {props.children}
        </SearchContext.Provider>
    );
};


export default SearchContextProvider;



