import React, {createContext, useContext, useEffect} from 'react';
import useApiService from "../utils/ApiService";
import {SearchContext} from "./SearchContext";
import UrlManipulator from "../utils/UrlManipulator";

export const TermApiContext = createContext();

const TermApiContextProvider = (props) => {
    const {dispatch, filters} = useContext(SearchContext);

    const [data, isLoading, isError, doFetch] = useApiService('terms?' + UrlManipulator.writeFiltersForApi(filters), []);

    useEffect(() => {
        doFetch('terms?' + UrlManipulator.writeFiltersForApi(filters));
    }, [data, isLoading, isError, filters]);


    return (
        <TermApiContext.Provider value={{data}}>
            {props.children}
        </TermApiContext.Provider>
    );
};


export default TermApiContextProvider;
