import React, {createContext, useEffect, useState} from 'react';
import Cookies from 'universal-cookie';

export const CurrencyContext = createContext();

const CurrencyContextProvider = (props) => {

    const cookie = new Cookies();

    const [currency, setCurrency] = useState(cookie.get("currency") ? cookie.get("currency") : {
        name: "Česká koruna",
        code: "CZK",
        value: 1
    });

    // https://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places-only-if-necessary
    const roundToPrecision = (x, precision) => {
        return +(Math.round(x + "e+" + precision) + "e-" + precision);
    }

    const convertToChosenCurrency = (amount) => {
        return roundToPrecision(currency.value * amount, 2);
    };

    const convertFromChosenCurrency = (amount) => {
        return roundToPrecision(amount / currency.value, 2);
    };


    useEffect(() => {
        let d = new Date();
        d.setFullYear(d.getFullYear() + 1);

        cookie.set("currency", currency, {path: "/", expires: d});

    }, [currency]);

    return (
        <CurrencyContext.Provider value={{currency, setCurrency, convertToChosenCurrency, convertFromChosenCurrency}}>
            {props.children}
        </CurrencyContext.Provider>
    );
};


export default CurrencyContextProvider;


