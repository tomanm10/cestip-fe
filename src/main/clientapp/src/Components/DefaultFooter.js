import React from "react";


const DefaultFooter = () => {

    return (
        <footer className="copyright">
            &copy; 2020 FEL ČVUT
        </footer>
    )

};

export default DefaultFooter;