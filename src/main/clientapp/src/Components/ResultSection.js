import React from "react";
import ToggleStatisticCard from "./ToggleStatisticCard";
import CountByCompanyCard from "./CountByCompanyCard";
import ResultTable from "./ResultTable";
import MinPriceCard from "./MinPriceCard";
import MaxPriceCard from "./MaxPriceCard";


const ResultSection = () => {

    return (
        <section id="results" className="results">
            <div className="row">
                <div className="col-lg-6 pl-1 pr-1 d-flex flex-column">
                    <ToggleStatisticCard/>
                    <CountByCompanyCard/>
                    <MinPriceCard/>
                </div>
                <div className="col-lg-6 pl-1 pr-1 d-flex flex-column">
                    <MaxPriceCard/>
                    <ResultTable/>
                </div>
            </div>
        </section>
    )
};

export default ResultSection;