import React, {useContext, useEffect, useState} from "react";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import {TermApiContext} from "../Contexts/TermApiContext";
import {CurrencyContext} from "../Contexts/CurrencyContext";

const columns = [
    {
        dataField: 'travelAgency',
        text: 'CK',
        sort: true
    },
    {
        dataField: 'country',
        text: 'Země',
        sort: true
    },
    {
        dataField: 'city',
        text: 'Město',
        sort: true
    },
    {
        dataField: 'food',
        text: 'Strava',
        sort: true
    },
    {
        dataField: 'price',
        text: 'Cena',
        sort: true,
        sortFunc: (a, b, order, dataField, rowA, rowB) => {
            // get only numbers from string price
            a = a.split(' ')[0];
            b = b.split(' ')[0];
            // compare number values
            if (order === 'asc') {
                return b - a;
            }
            return a - b; // desc
        }
    }
];

const CSVExportBtn = (props) => {
    const handleClick = () => {
        props.onExport();
    };
    return (
        <div>
            <button type="button" className="btn-icon"
                    onClick={handleClick} title="Stáhnout CSV">
                <svg viewBox="0 0 27 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M26.625 11.375H19.125V0.125H7.875V11.375H0.375L13.5 24.5L26.625 11.375ZM0.375 28.25V32H26.625V28.25H0.375Z"
                        fill="#475D6A"/>
                </svg>
            </button>
        </div>
    );
};

const ResultTable = () => {

    const {currency, convertToChosenCurrency} = useContext(CurrencyContext);
    const {data} = useContext(TermApiContext);
    const [travels, setTravels] = useState([
        {
            id: 1,
            travelAgency: 'Fischer',
            country: 'Česko',
            city: 'Praha',
            food: 'polopenze',
            price: 5000
        }
    ]);

    const getTravels = () => {
        return data.map((e) => ({
            id: e.id,
            travelAgency: e.trip.travelAgency.name,
            country: e.trip.accommodation.country,
            city: e.trip.accommodation.city,
            food: e.boarding.descriptiveName,
            price: `${convertToChosenCurrency(e.price)} ${currency.code}`
        }));
    };

    useEffect(() => {
        setTravels(getTravels());
    }, [data, currency]);

    const copyURLToClipboard = () => {
        const url = window.location.href;
        const elem = document.createElement("input");
        elem.id = "copyInput";
        elem.value = url;
        document.querySelector("body").appendChild(elem);
        document.getElementById("copyInput").select();
        document.execCommand("copy");
        alert("Odkaz byl zkopírován do schránky.");
        document.getElementById("copyInput").remove();
    };

    const sendLinkThroughMail = () => {
        const url = window.location.href;
        const elem = document.createElement("a");
        elem.id = "mailAnchor";
        elem.href = `mailto:?body=${url}`;
        document.querySelector("body").appendChild(elem);
        document.getElementById("mailAnchor").click();
        document.getElementById("mailAnchor").remove();
    }

    const printPage = () => {
        window.print();
    }

    return (
        <ToolkitProvider
            bootstrap4
            keyField='id'
            data={travels}
            columns={columns}
            exportCSV={{
                separator: ';',
                fileName: 'cestip-results.csv'
            }}
        >
            {
                props => (
                    <div className="card mb-2 flex-grow-1">
                        <div className="card-header d-flex align-items-center justify-content-between pd-y-5">
                            <ul className="nav nav-pills card-header-pills">
                                <li className="nav-item">
                                    <a className="nav-link active" data-toggle="tab" role="tab" href="#result-table"
                                       aria-selected="true">Výsledky
                                        vyhledávání</a>
                                </li>
                            </ul>
                            <div className="d-flex flex-nowrap justify-content-end">
                                <button type="button" className="btn-icon"
                                        onClick={copyURLToClipboard} title="Zkopírovat URL do schránky">
                                    <svg width="39" height="20" viewBox="0 0 39 20" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M4.3125 10C4.3125 6.79375 6.91875 4.1875 10.125 4.1875H17.625V0.625H10.125C4.95 0.625 0.75 4.825 0.75 10C0.75 15.175 4.95 19.375 10.125 19.375H17.625V15.8125H10.125C6.91875 15.8125 4.3125 13.2063 4.3125 10ZM12 11.875H27V8.125H12V11.875ZM28.875 0.625H21.375V4.1875H28.875C32.0812 4.1875 34.6875 6.79375 34.6875 10C34.6875 13.2063 32.0812 15.8125 28.875 15.8125H21.375V19.375H28.875C34.05 19.375 38.25 15.175 38.25 10C38.25 4.825 34.05 0.625 28.875 0.625Z"
                                            fill="#475D6A"/>
                                    </svg>
                                </button>
                                <CSVExportBtn {...props.csvProps} />
                                <button type="button" className="btn-icon"
                                        onClick={sendLinkThroughMail} title="Sdílet emailem">
                                    <svg viewBox="0 0 39 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M34.5 0H4.5C2.4375 0 0.76875 1.6875 0.76875 3.75L0.75 26.25C0.75 28.3125 2.4375 30 4.5 30H34.5C36.5625 30 38.25 28.3125 38.25 26.25V3.75C38.25 1.6875 36.5625 0 34.5 0ZM34.5 26.25H4.5V7.5L19.5 16.875L34.5 7.5V26.25ZM19.5 13.125L4.5 3.75H34.5L19.5 13.125Z"
                                            fill="#475D6A"/>
                                    </svg>
                                </button>
                                <button type="button" className="btn-icon"
                                        onClick={printPage} title="Tisk">
                                    <svg viewBox="0 0 39 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M32.625 9.5H6.375C3.2625 9.5 0.75 12.0125 0.75 15.125V26.375H8.25V33.875H30.75V26.375H38.25V15.125C38.25 12.0125 35.7375 9.5 32.625 9.5ZM27 30.125H12V20.75H27V30.125ZM32.625 17C31.5938 17 30.75 16.1562 30.75 15.125C30.75 14.0938 31.5938 13.25 32.625 13.25C33.6562 13.25 34.5 14.0938 34.5 15.125C34.5 16.1562 33.6562 17 32.625 17ZM30.75 0.125H8.25V7.625H30.75V0.125Z"
                                            fill="#475D6A"/>
                                    </svg>
                                </button>
                                <button type="button" className="btn-icon" data-toggle="modal" data-target="#settings">
                                    <svg viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M31.8875 20.2584C31.9625 19.6959 32 19.1148 32 18.4961C32 17.8961 31.9625 17.2962 31.8687 16.7338L35.675 13.7716C36.0125 13.5091 36.1062 13.003 35.9 12.628L32.3 6.40372C32.075 5.99127 31.6062 5.86003 31.1937 5.99127L26.7125 7.79106C25.775 7.07864 24.7812 6.47871 23.675 6.02876L23 1.26681C22.925 0.816867 22.55 0.498154 22.1 0.498154H14.9C14.45 0.498154 14.0937 0.816867 14.0187 1.26681L13.3437 6.02876C12.2375 6.47871 11.225 7.09739 10.3062 7.79106L5.82496 5.99127C5.41246 5.84129 4.94371 5.99127 4.71871 6.40372L1.13746 12.628C0.912462 13.0217 0.987461 13.5091 1.36246 13.7716L5.16871 16.7338C5.07496 17.2962 4.99996 17.9149 4.99996 18.4961C4.99996 19.0773 5.03746 19.6959 5.13121 20.2584L1.32496 23.2205C0.987461 23.483 0.893712 23.9892 1.09996 24.3641L4.69996 30.5884C4.92496 31.0009 5.39371 31.1321 5.80621 31.0009L10.2875 29.2011C11.225 29.9135 12.2187 30.5134 13.325 30.9634L14 35.7253C14.0937 36.1753 14.45 36.494 14.9 36.494H22.1C22.55 36.494 22.925 36.1753 22.9812 35.7253L23.6562 30.9634C24.7625 30.5134 25.775 29.9135 26.6937 29.2011L31.175 31.0009C31.5875 31.1509 32.0562 31.0009 32.2812 30.5884L35.8812 24.3641C36.1062 23.9517 36.0125 23.483 35.6562 23.2205L31.8875 20.2584ZM18.5 25.2453C14.7875 25.2453 11.75 22.2081 11.75 18.4961C11.75 14.784 14.7875 11.7469 18.5 11.7469C22.2125 11.7469 25.25 14.784 25.25 18.4961C25.25 22.2081 22.2125 25.2453 18.5 25.2453Z"
                                            fill="#4A5156"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div className="card-body p-1">
                            <div className="tab-pane active" id="result-table">
                                <BootstrapTable classes={'results-table'}
                                                headerClasses={'results-table-header'}
                                                wrapperClasses={'table-responsive'}
                                                pagination={paginationFactory()}
                                                {...props.baseProps} />
                            </div>
                        </div>
                    </div>
                )
            }
        </ToolkitProvider>
    )
};

export default ResultTable;