import React, {Component, useContext, useEffect, useState} from 'react';
import {SearchContext} from "../Contexts/SearchContext";
import CustomSelect from "./CustomSelect";
import Slider from 'rc-slider';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';
import {FilterApiContext} from "../Contexts/FilterApiContext";
import {CurrencyContext} from "../Contexts/CurrencyContext";


const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);


class MobileFilter extends Component {
    render() {
        return (
            <section className="top-bar-mobile">
                <div className="logo-mobile">
                    <svg viewBox="0 0 59 70" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="59" height="70" fill="url(#pattern1)"/>
                        <defs>
                            <pattern id="pattern1" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlinkHref="#image1"
                                     transform="translate(0 -0.00121359) scale(0.00485437 0.00409154)"/>
                            </pattern>
                            <image id="image1" width="206" height="245"
                                   xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM4AAAD1CAYAAAAYosqqAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z13uFxV1YfflUgvoQlKR0CKdDAUsYAigiCIIKggKqIgtk9RLIiiIipiwYI0adKkSrMAUqXXQBI6SUioIY0kpP++P9bcOPfeKafsfc7M5LzPM09u7p3Ze509Z5+zz9pr/RZUdCSShkv6j6Rpkg4o256K/rypbAOKQNJiwIbA24E1gFVrr6HACgPePhWYDEwBxgPPAk+Y2asF2vs54M/AYrVfDQcuLar/ivb07MSRtD7wKeBDwHb87yTM2t5TwJ3Af2v/PmFm8/La2aCfg4EzgCF1v34ydD8V+bCyDQiNpA2BnwP70v/kC80c4HFgFPAUMA4YW3uNM7NZaRuUdDjwJwZf0PY3s8vzmVsRkp6549SWYz8Avg0sUUCXiwNb1F6N7HmR/02mSfgScAowrfbzHHyZOBRYC9gT2KpJX9sC1cTpIHrijiNpPeAS4J1l2xKJ14AtzWxC2YZUODGXMoUgaThwF707aQBWBq6XtGzZhlQ4HTdxJL1J0gaStpW0Wpv3fgS4GWj5vh5hC+B7ZRtR0WFI2k3SZZKmqz/PSvpxbTnW994hkr4haZ4WLV6W1BPL626n9C9B0rbAH4Ad2rx1AXA77sHaCdg0smmdyhZm9mjZRizqlDZx5F6wE4Bv4J6limQcZGaXlG3Eok4p7mhJawKXAduX0X+Xs2bMxuUOiPXxO/pWuEv9tBibvd1M4RNH0pbAdXjoS0V6lgzRiKQhwNb4xWs7YCN8wjRytBwi6VAzeyJE371AoRNH0m74nWb5IvvtMd7I82FJGwGHAZ8k+cVre+BBSUeZ2Tl5+u8VCnNHS/osfqepJk0+XsjyIUnrSToXGAl8i/R3/KWBsyWdLWmZLDb0EoU4ByQdA5xYVH89zvpm9mzSN9fc118CTgKWCmTDKOAAMxsVqL2uI/odR9LReNBl3knzALAHsA9wJjA/Z3vdyG0pJ82ywNW4uz/UpAF3HNwr6dCAbVb0IenrATf//m9A25+SNCZg+53ONEmJ964krSrpvgLsOkVSzwQLJyXa0knSl4FTAvbxOzP7eoN+VgE2B9bBk9PeAiwDrFh7y7J4Ls7SeNT0Urhnasm6n0NejWPwKnCgmd2c5M2ShuF5Q++IatX/+Ce+vzS1oP5KJ8rEkXQEnlcSsv0bzWy3gO31Q9IS+ORaHFgOGIZPvuXrXsPqfl5xwP/7/r5cQLMmA2cDv04aGV27+l8PRBurJjwG7G5mmZwX3UbwiSNP+z0zQtsjzWyzwG0GR74/ssKA17AG/y5d+8hQfNLNwPN2JuMp2w8Ao9NuPEo6gfKCQZ8BdjOz50rqvzuRtI/iBV4mWqYsykjaRtLcSOOflHHyyJCeJphXTdLOwEXEizu7M1K7PYHc7Xw65Wf1rgVcqx7PHQoycSRthrs9Yz1kzwPOidR2r7A3nmLdCWwJfLpsI2KSe+JIWhv4B//zYsXgN2b2VMT2e4FjyzZgAFXEezMkrSxpVOQ185VaBPcJ0iDPlu00xktap+yxiUXmO46kpYFrgU3CmdOPqbh3aP8qpL0tnyrbgAasAdwv6SuSFi/bmNBkchlLGgpcBewVwIZ5wH3Aw/hG32TgaeBmM5sRoP2eR9IDwDZl29GCKbirej5wopldVbI95SDpDwFu5bMlnSDprWUfT7ejwToNnczTZY9XCFI/O8hjxo7K2e+rwJ5mdn/Odiqc6XiYUTewrqTFzGxu2YbkIdUzjqR9gV/l7HM2sEc1aYLyt7INSMFQPK6wq0l8x5G0HXAB+V3YPzSzB3K2UdGfb+EXpEOBN9f9fj7wMh7C8xoeR7cKXrmhTHfxTvhzbNeSyDkgdyvejUce52EcsFEWQfKK9sjj5FbDN6JnAq+Y2YIG71sB2AX4OPAxclZyyMCTwDY97fyRNEzSY4EeDI8p+3gq+iNpLUm/kjQr0HeclNPKPvZoSFpM0r8DDdQctZG0rSgPSetLuirQd52UvE6mzkTSmQEH6bqyj6eiPZL2kzQp4PfeirmS9ij7mIMi6WuBB6kTd7crGiBfvt0a+PtvxjRJDWsMdR3ywq1zAg9OT4eZ9xryqhGnBTwHWjFWUl7HU7nUBuyhwAPz57KPqyIb8pXH/MDnQyPuVzfrtSn8Ek2SOjmOqqINkj6pYkqqXCl3qXcXkpaS9FLgwbij7OOqyI+kg1RMWvbJZR9raiQdEmEgestrsggjnzxFLNs+V/axpkLSPwMPQBWP1mMorMhkM2ZIKkoTLhMLQ27kiWmvEaiMRI39zOzKgO11JJKWw1PH38RgAcR6Gv0uFJPM7KZIbfdD0inAVyJ3MxIYbmYzI/eTifqJ807g3oBtj8TL7g2KlepG5B6f7YB34XVl3lr36gQlUAF7m1n0jWZ5IuO/gV0jd3W6mX0xch/5kHRg4NvtgWUfU14kLS/pS5LuUPl6ZUmYKBdPKWJsVpf0SuTjWSBppyKOJy31rr+QG5S30V05Iv2QtISk44CxwB/xu0w3CIasDFwsr68alZrU7WdidwP8UX6H6yjqJ06oEO+5wFFmpkDtFYqkdwOPAMfjcrXdxo7Az4royMyuBy6P3M1WwCGR+0hN/cRJXHelDaeY2WOB2iqM2l3mj8CteD3Mbuab8rKRRfAtIHZ+1Wcit5+a+onzCPB6zvbG41fqrkKudXwbXrmsF6rGGXCOPGEtbkcusH5u5G7GR24/NQsnjpnNJt9ziYAjzCzv5CuU2tLsfmB42bYEZnWKu4j9JWLbzwNHR2w/P/ICqzMzekBOKtv+tEj6vMJGgXcacyVtXNBYxlB0naoOjXPsF1BXu+0eid890nAt8N1QRsVGksnryJxB8fn2RfImiquVc3Hg9qbihaoeDNxuPCR9RsnvPNfKow66ArkT4IIIV8dOZbakmIL4feO6ocLFsU2WtH1sm6NQG4iL1HwpM0PSseqiMHBJK0q6JdCX200cVtD4Xh7A1snyKJaOpq0HSdJKwPvwQqxr4fpdTwMXmtmrUa0LiFwo5N9Ab6TppuMxYIfYckyStsYdLVkvphOpxCo7B0lrSBod4GrYzdwtaXt55baYY31KRvtGS9ogpm0h6YU9i5ZIWhe4CXhbyaZ0Ci/g2w4/N7OXQzcu15Z4EFcLTcpNwAFmNjm0PRUZkD+njQtyve49JknaP+K4T0xoxxkqILYuOpKWVeTbeRFIWk3SczHOuB7jTEUQyZBXwJ7Qot/Z8soX3Y2kHSTdWzuoEepWdyALtRPujniy9RqPSForwvewphqrg45SwI1N+bPblyVtrSIv+rUOZww4uLmSvlyYEYGQNETSpZFOsF7mBUlRqlbLa5T+UNJJkg5VoJqu8lXFwIk5XtKJiq3TJt9Fv7PFgJ4lKVa6b3Ak/Tzk2bSIMV3SPmV/h0mQtI+kl1scyxuS/ix3DkUxYNcEA3qnPIK4o5G0pzxrsCI78yQdXPZ32Qz5iuIXKY7nDUnHK3R0i7zEQxJelbRn0M4DImkl+XKjIj/zJX2+7O90IJKWkYsWZmGMAt5NhwA7J3zvKsC18tneiWnEf8KFMyryMwQ4XR1UhkPSGsDtwL4Zm1gHuEq+fMstrmKSxuGhNGm4Ezi4Fk1dOnJhkNDRuRUeJf91MzulVCOktwE3AusFanIErgg0LmsDJmk2sHiGz87AQ9b/UKYElKRVgMdxoYqK8Ag41MzOL6VzaRPgBmCNwE0/i+u2vZblw0OAeRk7Xgb4HXCPPIuyLH5ENWliYsBfJO1ddMeStgJuIfykAQ/B+mXWD5uk8YQx7GLgG2b2YoC2EiFpU1wroROfuXqNN4APmdltRXQmaUvgP8BKEbuZYmaZcpWGAKFSAw4CRks6SsXt3v6KatIUxVLA1bWLVVTk6d7/Ju6kAU+RycQQ4KGAhgwD/gBcFHvySPoQUFVCKJZhwBWShsXqoM4RsGqsPurI/Nw2hLB60X0cCGwWoV3Aq8bhd5uK4tkIODfGhVHSW/EUgxjPNAN5HPhx1g8PwX3jMYj5wH4wnpFaUQ77AN8P2aB8Z/9qYN2Q7TbhSTzTNLOU2RAzGwk8HM6mhURZn8pzN34Qo+2KVBwfypsq1604D68GEZu7gHeZ2Zg8jfTlhp+X25zBxHqwO5Qqm7MT6IsuCBEAfALwsQDttONM4P1mNjFvQ30T5wJcLD0kwSeOpMWBY0O3W5GZjcm5ZJO0O3BMGHOaMhn4uJkdbmZvhGhwCICZvYKvL0MS447zOTzmqKJzOEZSJkdQLerjbOJqX9wObGVml4ZstF7G54yQDRN44tQ8aUWpUlYkZ3HgFxk/+3viBebOA44DdskTk9aM+olzIxBS9ST0HWdf0gejVhTDHnJNtcTUNjljVe0bA7zHzH5iZvNjdFBfrWA+cFXAtkPLrnZMiHvFIIz02uFbEmeJdh+wvZndFaHthQwMV7kcCFWsNNjusrx093tzNjMSvzA8DIzGQ42WxZcam+OVv3YA3kMVxpOFj0na2MweT/j+ZyLY8DS+P5Mp4jkNA0+QW/F0gRByQSFTVfch29XpddwFeZqZPdHg76/U/n0cuBQWSv7uDeyPh/R0XP3JDmUIvlWQ9M4zGlhAdrncRhxZxKSBwWU+5hAukiDkxEmrd/AsXmJvLTP7RpNJ0xAzm2Rm55rZ3nji1E9w9cuK9iTei6npWI8I2Pd4PFynEBrN9hsDtZ0lOa4Z5wFT2rxnAXAd8GFgQzP7lZlNzdOpmT1vZsfhLvADcUHxiuZsWEsHSMotAft+tsiCzY0mzj2B2p4TqB3M7G5gA7w030PATDw/ZAy+//RV4G1mtpeZXR86I9XM5pnZ38zsncCuwPWkL761qJAmAuCWgP2+FLCttgx6bpC0PH51z+vxeNLMCqveLGlJvI7nxsDb8f2BZfEl4zz8eec1/AFyNHBPnvWwpM2BHwL7sQiI16fgdjN7T5I31p4nXyXMc87lZhZFC7sRg7xHZjZN0jP4FT4P0Yvo1oIDPwZ8Fve6pXmumi/pXuAK4Jy08Utm9iiwf23/4njcoVABW0kakuSub2aTJD0EhFAQXS5AG4lpNtNDJLdNCNBGUyRtAYzCS1bsQXpnxFBgR+Ak4HlJf5S0elo7zOwhM/sI7sr+d9rP9yDLka7Exz8D9btZkbJlzSbOAwHajlabXtIHgTvwpKoQLAl8CXha0k+VQfXRzO4xs92BdwOF5OV3MGnuIP8I1OfqwIVyNdd3Slq7FhQchWYT5+4AbT8SoI1+SHqLpF/iVa4H3ppfAT5JvrChpfBo35FZVR/N7A689OOnCafn0G2kSTK8B49eDsEBuGf1XmAs8Ia8EsPvJO2i2PVqJS2uwdUL0rJVIFuGSDpfrkLfjKckrV97/8M57a7nGnkOfFbbV5Z0Q0B7uoWzUo7TJQXZNUHScfKo7Fw0nIEBNkLHEm5zy4BdaJ6Hfhuwk5n1hXDkTlKqYy9ghKRPZ/lwzWu3B57vtCiRVmgj1HNOO1bHHTljJZ0gL7uYiVa3rr9nbRT3UgXZS6kFn+6Gy+7WMxPPHPzAgOrXD4bot45lcHGKs5VhzWxm84DDcTf4okLaiXMdECWKuQlL4ykqD8udTKlpuv8gVxwZT3of+xhgczObnsWgVsg1vbbFNz9vbVQuXtJGeHnyGB6Wy4CDsoSqy5/NvhXepI5krJmtm+YDkm4neQGAkEwCdjaz0Wk+1HRS1BQ5b03R1gJcCOGDMSZNzaZRZna+mV3WaNLU3vME8O0Y/eOBn1kFKhaliOvlM3wmzwonDysBl6RdTbS7myR5yBuHe7NWMbOdzOypNAbEwMx+g3tYQoeuT8H3jlIh1yDbLbAtnUwW71XIXLC0bA4cmeYDLUNF5HVExuG1cRrxGK4a8kqTv2dGHvqzNjCwluPUutcrrQL7aifsO3CtrtXwnJuPkk3wbgTweTO7L+0HJX0AV9xfVHjdzFLfdSQ9SkQhyza8DKxjZpllcftRc981YoakvGE5A/taU17D89GE7sUZcpdx4kKpclf79+TFget5RNIxknaSx1D1VQDbSDkrMtdsXJTItFSX9JOS7f5cUlvbBidKWhH3CA3UEDjGzDKXSRjQh+Ebj9/DNyHT8rW0xY8kDcdTBaYD15tZqKjwRn1NwF2hebkX+DPwX9yruAYeNvRu/MG6CL3lJLxhZqmjL+RVr8tM3bjbzHYM1pqkIwfMzLvlipoh2l5M0sU5rxQdW+wVQNKonMc3VV7mvJUX1CTtLC/VNylnf3mZkWOsni3Z9kRhXEkf4k4DLqn9/CDwETMLJWB4EvnVTkKFbMQiT3LgSGC7WlZq0+c5M5OZ3WFmR+ApFfvhQadl5A3l8apeFsyKbBwStDV56Mv2CiN52tfmfoGuEll1vaIgaXlJP5J0naS3S9pU2crI3ycpl3i9pM0knSVpVqCxTsKTOewdXqCdjRij2DFteZC0tKRxgQ52jqSvKNDyMedxbSfp+Trbjqv9/raUx3SlcoSENLBrNUk/ljQlwHi3I7Xnsc5Ok5+8ZbJLqHEPjvyKHJoXJZ0r6WhJn5a0h6TNVdCEkrSjpNcH2DRO0u6Sbkl4DFPkVe2iXPUkrSTpZw3sDEku0QxJJ0e0LQnnhhrvoEhaTtLkAgdipqS/yr02sY5pPWV/KJ8v6Qm5678Qz5ikN8tP0JkBxncgV+S0bacINqVhhnwfsbOQ3xHK4m+S1g18PEtJeiCDLftKWkW+0VwK8r2zC5XtGawZZ+a0ydR/uVsGX2hlY+EPQfIlyFeL7reOA/Aiv99UuOXQ6cA2GT53ODA5VOmJLJjZeDP7JK7ZEKoebK5Qp5r3MNddKwCJN0MLQdJ7S72O9OdW5UhUqx1P3rvnSaHGNi+Shkr6oqSJOY/pgAC2vDunDSHonHKZkk4tezQG8LoylpiX9H2FWeIUUY0sMXIP3OU5jidV5YImNgxROK9rVk4OMZ5BUOsU6DK5VVIiYXe5Z+rsgH2PkdRxGtWSDpL0aobjCSLVJPf+lcnLiij4kWYgNi55IJJwp9yVPSiCWi4W8h3FCWnZqYzvpB3yu88VKY4jmCyYpE0ijHNaDmpkWzDngKRlJV2l1rE+u4bqLyI7AucC4yU9Lel++Q7+k7hW3ImEr/0D8SqT5cLMXsZFH4/GFVHb8d+AfY+mfL3uoxv9MqRX7US8HMcISZdJ+ogGh+d05FW1Bevjqdrb4SJ7Mb2Qz0VsOxe1OLiT8WS8dvJbNwfu/vzA7aVlW0nvH/jLIJrHkjbEMyMHpgdPBvYysztr7xsFbBKizx7jSWCT0GLxMZAvYS/DlUsbsUmK4lJJ+nszfqcvM5xqBLBtTXgFCHcFPZbGOfUr4gVMkcddFSbC3mUc1w2TBsDMJuCCi2c3+PMLISdNrb9XKU4+qhlbAF+v/0XuiSNpTVxzoBl9D9mbhuivB7nMzC5p/7bOwcxmm9nngK/R/7nnwkhdlr1cA/i26jyfIU7kL9BawaVvTbx+gL56jUfwSgtdSS3r9oPA83g9pFMjdfV34MVIbSflzdQtT3NNHLmP+/A2b+vTkK4mTn8mA/vFktIqCjO7GV9NfNDMno3UR8xJmYaFss557zj7M1iFZiB96i65BC96kM/GOtGKxsymm1kaDb4snAaEUaDJztv7fsg7cdoFa07jf6KGuYWue4i/m1lZAnxdSU2C7OKSzRjW90PmiSNpe2D7Nm+7qC7yN1cKcA8xC/hGiIbkkRjXSHpOUlaF0W7idyX3v2TfD3nuOElSA+qVQDsuFqsk/hJiiSavQXonXlFhXeBDedvsdMzsIfJV0cjLQvWeTBNHLsjerlDpiAGql0nCNXqdubiqTy7kgomX0z/0Z2zedruEn5TY97S+H7LecY4C2kWNDtSdjlbasIu4zMzG5GmgtpN+NYPrbAbdeOxUzOwGwpZ5T8PDfT+knjjyalbtlmmzgb8O+F1VWBbOyfpBeZLZIXiW5rsavGVRWgofg1fHKJKHgUv7/pPljvMj2pfGvt7MJtX/wszOxyOPQwkZdhsTgFTqL3Itg69I+hu+kXwezQXjM9Us7UbM7F7gjAK6mo0Xvfo0sKOZzczUiqStlSzjcb8WbdycPTWiq0mdTSgv+pqU+ZLyKqJ2DZJWVLwM0TslHawWSjdpix1tS/uI6qn4LG3GKDxIsCgW4CEhE3Fp1jeA1/Fo25Vrr1WJv8+UJR4tTRrGEOAieRbrD2r1R3sWM5ss6VPAfwhXtOsuvJhAWM+dpK8mmK0tdZIl/SLSVaKeeXLx8fcqofSSfE8ki8RTEh5XNk2DuzL2N0XST+UBuD2NpM8qv+7DaEkfTdNv2mecJLrRD7T5e5AcoBY8CQw3syPM7Nak0ku1cPj34g+eoT2AP2slmN6C1zP2Nwwvm/KcXHRj1ywTtxsws7PxQOM5GT7+TO2zm5vZlUENq0fS1xLM3oY52nVtnJTz6tCKvymAUISkN8nlc/8qaXpOmx5URiEOSX/P2Xc9oyR9SQH1qDsJeUGA+xOMwwz5efJhFSWuLunzCQx7X5s2zsj4xbdigaTjFeGqKmlJSXvKZa3SPoxOUI5qbpKuDTpKzhRJv5a0Xshx6gTkklLvqx3fDfKJ9F+52Mjx8oth6oJXIQz7ZIIvZnibNq4LfCLMlvTpAsfgbXIVnDPkd5NG2svTJV2knM8YkkYGHquB43aypBVCjU1FE+Sq++1oqX4oP9lCMUlt7nBtbFlS0lp5Th75VW49SdvWXltJyu3lkU/QkHrOzXhZXty3IhaStkzwRQwMBRnYxiuBvvBnJG2c0v4l5ELnp8s9XfU8pAAKlCGQF/i9M9A4JWGWPNq9IgaSVk3wJTQVH5cr84dgpDzQNKndm8mfUV5r0+6TKmMNPNjeHQKNUxquLfu4u4m0S4qJeJRzq88Na/G3zVP214iH8TTdV9u9UdIeeO7L+0nmBt8QOEPS18xsoqQlgQ3wtO+34eH7d5vZRVmNT0i20I58vFfSYgFru1bUI+mlNleufVt8NskGaiselpePb2fjh+SVsfMwS42fMWYp8p6I3B3e7u4Yg6ahUnV2fVTSBZKeUifXygyEfHk/XF7RO7uOtKQRbQb/0BafPTPHl/qKpHXa2PY+ufsxJpNa2RAKSedHPo5GjFCDk0O1yGz5UraPXDVwOh25c+ZP6l8z9VnVqvpl8f60WyK18lDlkcA9xMwaJmtJeidwAi7RGpuH278lCJcCBxfUVx+bA78Gvtz3C0lb4lHZWwx477qS3mpmZcs2AVA7oQ8CNsNzxaYB44A+/ekRNbWcdu1sDXwL+DiDUzXWA26QtF2MibNSE4NWBVJ5wep40sz+1aDN4cB3gH2JH8rTR2ht5GbcigeoFr0cOkoeXfAjXNXlUqBRlPAQ4EhqSq1lIulE4Nu0Hqu5kh7BJ9F9+ISaVHutAAzHNe4G6UQPYEXg+CxG/r7N7f4PTT73sRxLiKmqLdMkLS/fgLwnR3t5KEwUQ4Nd5p3G65JWK2o8mozRd0o47peyXM3a3XGaqdm8J0NffSwPPClpLPAaXoajZYRCJGYC9xbY37gC+8rCsgRS7MmCfLO9DA2CYUVOnL0y9FXP4sDahMu9yMK9ZtZQFE/SMgoQMTCAjnh+aEPW5XcIfkU558O4LBNnapu/D4oElj9w5SpS2yEMKpokD9s5E38YHa2wOTDdMHEuL6NT+UZ7WZJYN2WZOO02yBrlv7TcH+giRtT/p3aHuRI4DH8w3QD4bcD+ZrR/S6mcZ2bnldT3N0vqdwHw+yy3uflt/j6rwe86qqpyDl4a8P8fMfiqt6+k1WolAPNShvrpXLxw1D/wNHcDtgT2rL2WxJPGfgt8rwT7+nTl2un6xeJqMxudZeK0S4R6vv4/knahd6qwLRwveTjPdxu8ZyiwCzl1juXRCYmqYAfkOuArZjawrOL9wFnyJMH1gfFmNrFg2+rZn/a6fjGYjNcEyvRg1a46wUD9tK9l6KMd04GR+NXwHcAyEfpoxHbAf+SbbefTfN/g/bSZOJL2BIR76qbhz45T8O9kXXwTcqtmn4/AsbRJ8Taz1yluA7gVrQqZxeSLZpbN0ynp+y382/3ShCWtL5ctCsUCSb9UnWyPPJbojwH7aMV0SVfL029b0XZwla4Eemx+nOlkKAFJayrsOZWUn+c1/P1NGp4lFwKvf++fAxt/VBOblpP0SOC+8tJy30rSSpIeK9tISf9RFwl5SPpECWPUcFM/reGmxpHHRwx439ry9NxQtAzll7S0PK98WsA+8/CfBGO5mlyaqCzmSnp7Ozs7CUnHFjxGpyrUhUXSWyXdV9f4oFt9rcNQzFJC0QtJw+TpCzcq7MTNwiEJ7F1dHqJfBldn+f7LRNLXCxqbuZKSlLJJfQCLSTpM0qCIAPnJMCvgQVyW0cbl5Lk5n1ItzVrSypL2lnSWpMkBbWzEDCVIx5a0rqTnI9vSiK7Tm5ZftF+IPC4TJRURaT/o4H4U+ECieFHkjoXdJf1c0h3qn3sRiimSdk1gy8Zy8ZGimKg8iVklIk/B/46kJyKMyxWS2nmOoxzUYnI9sZAUVrFarnqzu6SjJZ0jdzrMzWn/bCW47ct1v4ryGOV/4C0ZucLQzwONx8uSPl7mweRJH2jEAjUJnpSrwaxawDEtJemD8uoBeRTyL1Cbq7ykE/IOWALmSuqF2ME+Z9V9bY63HbeojLvMgAO5OO+3OoDpDfp4j1wTue856i5JP5YrbjarHxPq+IbKJaayfln/kouANGt/ScVZgtQzsOhXVyMvIJyVvytDVHtQ/72kJYBXaJwxmJUFwOJmNl/SB/H4qHahKNOAJ2qv0bV/H8czSYOouNQG+7t4vFraYNnLgY+bWcOqYnLl/CtyGdicWcCmDcJqOh5Jy+DZlzvjikuj8XPt82TLlB0F7FCLiCgP+XImBtcrjBzsbPkzywXyZ5idlPMBWdIuynb3+XqLNoeovzBGSH6a53jLQr4kuzHwWOxe9nEBpWxOiE04eQAACqdJREFUhWCmpFvlXprMwaiS1pFL4O4s6QuS/qHWErbTJTWNfpZ0XIRjfVwdILiYBUl7BR6L28o+poVIuizwwZXBSEmfUwBXraR3SLqqRV9fafHZ7QMf1xxJ2+U9prKQrxJCcngee0IrqPRC+sCmeKn5EfLUgcyY2Ugz2xc4kMbZnK08W48QtrLy8WZ2f8D2iibkhuQC4Jo8DYR2DkwAVg/ZZgfwGC6R9BDwgJm9kKUReUT3D4Av8r+q3R82s+tbfGY8zatMp+EeYGczmxegrcKRK+kMTCLMw31mVobYS2OUv3rZQKap2N30diyQ+/z3V0b5V7m81Z5qIU5f995RAWyeqzalVzoduUJrSHIr44ReqoW8oj0H7EidqmQH0JeVeSkwUhlivcxsmpldb2YPJuwvL2ea2cgA7ZRJ4soUCRkkbpmW0BPnlQBtLMCfMbaufeHXEXatH4qNgavkJfNaalrnIG+9ztlkUZ3sPEKKHk4F7s7bSOiJk2cdOhP4C7CVmX3ezKYC1P59vuUny+UDuCOhbQpBGuSZtHnDQC40s5DPBmURUrTkphDPeqHF3B4E0kjEjsflla4BbjezRgo5AE8Bsa7qIVgeOFfScmb2p0BtrkH+7+fCEIZ0AEsFbGugJkYmQt9x0twCzwE2NLOvmtkNLSYNwNP5zCoEI6xc0rYB2rgnQBudQNPYvgzkfr6B8Hec20imsP8o8IUUcWNj8hhVxzN4fNloXDhxVXw5tD4uJrgBXpVtTbJdVH4dxkwgvzb2vNJjsMIRauKMMrMxIRoKOnHM7AVJd9BeYP3LKYMtG9bFycCpZnZp3f8bFkeSRy+vj0+ivgm1Fu7deQvwZvqP3TTgZOA3geyE/Jpq04JY0RmEkv8qRa43EZKObONDT63LJWnHQP77ILVt5AGHq8mrdq2rOkmsQO1vE+BYuy76uRmSrg0wHpIXyepM5LJHrXTHUgsUyjUMQvGFGMcdCkkrKIzU1SNlH0soJN0WYDyeCmlT8GpfZjYJuKDJn2cDWZKoXqp9NgR/kOf1dBzykox3MbhsYBYmB2ijU1iu/VvaEnSZFqtM3m9pLM5+pZm9lraxWsLX+NxWOYsBf5dL0HYEkraUdCXuBQtVb+bZQO10AiHS4zv3+aYeSWcMuFUukNfszNrezQFu1/XMVs7Q8jzIU7D3kq/fW+XtZKW0SmkhkZeIn5dzLMaoW9RKJb1F0qt1xl+Ss72Lcg5eM85UCw2AkEhaTy7heu6AsYlBWz23bkCuOpSXkNsEQMQycGb2kqSDgWvxgM0jczYZK3TkMOA9kr5hZtfW/0Ge474RXkl7KL6fMKX26tsj6XuWWB4fzxXwEJG+PaIN8OrNm+Fu7CJ42MweKqiv2CRScG1Ds2fuzEStn2hm/5K7AF80s7wPqyEKNTVjQ+AaSY/j5dgXwxPahlNuzdGslFLwKRKb5fz8CDN7IIgldUQ/KcxsVKCmighW3Jhyi8GG4Ddm9o+yjQhIXg/jOSGMGEgsr1oMYt5xeoXzKK82ZizyxOzNJcIyDbpr4vRCeHxMrgAOa1VRrduQV/DOE7N3vZmFyBEbRDdNnOqO05xTcIHDrtQUaMEnyHeOnhPIjkHk8m1L2gw4CE80u8HMom26yePBZtGdD+uxmAN808y6XkC9EZIew2u8ZuElYO1Qyq0DyXwSShoG3EJddp6kG4Cvmtnj+U3rT00C90XCuCd7gaeBT5rZfWUbEgO5nHIeR82psSYN5LsNfoLBKa27AfdL+kiOdlvRySnURTEH+BWwTa9OmhrL4HtnWZgFnBrQlkHkmTgfavL7ZYArJO2do+1mZCuV3RvMxEVMNjWzb/VQklozptM43jEJp5vZqyGNGUieidPK2zEUuFjh8x8eDdxep/MycAnwBWCNmohJw+S7XsPM5uDVBNLyOvCzwOYMItMzTs1N2E7ramngQknbmdkbWfppQBItsjIR8E9cfGQsHtFdX99ncfyOvCI+PkvgoTp9S5JpePrE88BzWSLJe4xrgM1TfuYIM+tMD6ykj6YIsPthwH5XUvmVpBsxQ9IlknYMdawVC4sKv5Hie/h+2Ta3RNJPUxzMdEnBlBhrJ2gn8JCkn8nr4ywR6vgq+iPp+wm+i+mSDivb1rZIujrlSRZszSnP9f+OvEp00bwmL9a6bqjjqWiNXN/hZ2pcwHiOpPNVQj3TTBugksaQTiDwNWDNNtppWex4L3A6HrYfk1l4VuuJZtZL6jFdQ21y7IlLd83BJb7+VUvV73wkDVO2jMW9ItmzpKQTm1yRQjBaUtoH1IqK/kjaLOMJ+JfIdm2tsDUi50k6TVJe4fOKioXFYrMwRVJIDeBm9m0o6ZtyjYKZKW2cI+l2Sf8nae3YtlZ0L6mfcZSvlPjhZnZmxs+mRl78aR08/XltfP9kOXw/ZQGeAj0DmIBvtj0TM76pYhFG0q4Z7ziSNEFdWvW4oqKeLCE3efTNVsfrYFZULHrIS5pnZZ6knco+hoqKPGQN8jwrR59Dgcuqh++KRQ5JK0uan+OuI0mPSQpZoq6iovORNC7nxJGkeySFENSuqCiUTEs1uWTsSgH6Hw5crQL2dyoqSkVev+XsAHebeq5Qt4hiV1QkRR6fdrmksfLd9RgcW/ZxVlQERdI+kSZLPfPVQTVrKipakWh5JGl54CEgdt7DROAdsdQXKypCkcg5UMtB2REIrpc2gFWAnhTXq+gtEnvVaneBc+KZspADJB1QQD8VFZlJ644uqq7kWZI2KaiviorUpJ04E6NYMZjlgMslrVFQfxUVqUg7cYqsbrAJcLekDxTYZ0VFItJOhKxavllZE7hB0oVykfeKio4g7cQp6+T9BHBHNXkqOoW0E6eoqsmN2Aw4rcT+KyoW0k0TB+BASTuUbENFReqJk0aEMBadL3Va0fOknTgbRLEiHQfVQoAqKkqjGyfOssAvyzaiYtEm8cSRpzkHqzqQky9KOl0BqyBUVKQhzR1nm2hWZONwYLxcefMoeVZqRUUhdPPEAbd/Zzyi+hlJB5ZsT8UiQpqJ865oVoRhdbzu6KWqBEAqIpM0kW1xPMCzW07Ix4B9zKyoaO6KRYykd5yd6J5JAx5lcK+k3cs2pKI3STpxPhHVijisDPxDrsizetnGVPQWbZdqteeFCXTXHWcgc4ArgTuBR4E7qnIeFXlIMnGOAE4twJYimQT8GTi5a2pIVnQUSSbOo/gzQy/yCnA08FczU9nGVHQPLSeOpO2BuwuypUzuBf4IXGNmk8s2pqLzeVObvw8vxIryGV57zZc0EhiHF9B6Ep9MT5dpXEXn0W7iLGplOIYCW9RefZws6SrgS2b2UjlmVXQa7dzRMwqxorMx4KP4vtAW7d5csWjQbuKMLcSK7mAt4D5Jv5W0pbyidcUiSjvnwIb4Or9iMLNxgca/AidV+0KLFknc0WOBql5nax7AY+MmlG1IRTEkWW78K7oV3c+2wE2SVivbkIpiqCZOODYCLpXUzlNZ0QMkmTg3AtX6PRnvBo4p24iK+CTNx7kJ2DWyLb3CDGBDM3uxbEMq4pHUpXp1VCt6i2WAD5dtREVckq7HLwc2zdD+MsDiKT+zGC4BlZZhtL4QtGrXgBVafHZ5BgvOTwPm1/1/MvA8vvf1YEtLK7qe/wf1HoQcQ4EPWgAAAABJRU5ErkJggg=="/>
                        </defs>
                    </svg>
                    <span>CesTip</span>
                </div>
                <div className="filters-icon">
                    <button id="close-filters-icon" className="btn-icon d-none">
                        <svg viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M27.7083 9.34791L25.6521 7.29166L17.5 15.4437L9.34792 7.29166L7.29167 9.34791L15.4438 17.5L7.29167 25.6521L9.34792 27.7083L17.5 19.5562L25.6521 27.7083L27.7083 25.6521L19.5563 17.5L27.7083 9.34791Z"
                                fill="white"/>
                        </svg>
                    </button>
                    <button id="open-filters-icon" className="btn-icon">
                        <svg viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M14.5833 26.25H20.4167V23.3333H14.5833V26.25ZM4.375 8.75V11.6667H30.625V8.75H4.375ZM8.75 18.9583H26.25V16.0417H8.75V18.9583Z"
                                fill="white"/>
                        </svg>
                    </button>
                </div>
            </section>
        )
    }

    componentDidMount() {
        document.getElementById("open-filters-icon").addEventListener("click", function () {
            // TODO animation
            document.getElementById("filters").classList.add("d-block");
            document.getElementById("close-filters-icon").classList.remove("d-none");
            this.classList.add("d-none");
        });
        document.getElementById("close-filters-icon").addEventListener("click", function () {
            // TODO animation
            document.getElementById("filters").classList.remove("d-block");
            document.getElementById("open-filters-icon").classList.remove("d-none");
            this.classList.add("d-none");
        });
    }
}

const DestinationFilter = () => {
    const {filters, dispatch} = useContext(SearchContext);
    const {data} = useContext(FilterApiContext);
    const [searchOptions, setSearchOptions] = useState({
        options: [],
        inputId: "destination",
        defaultOptions: true,
        filterValue: filters.destination
    });

    useEffect(() => {
        // event is value
        const handleChange = (event) => {
            dispatch({type: "SET_DESTINATION", destination: event ? event.value : ""})
        };

        const getOptions = () => {
            return data.destinations.map((e) => ({
                label: e.name,
                value: e.name
            }));
        };

        setSearchOptions({
            options: getOptions(),
            onChange: handleChange,
            inputId: "destination",
            defaultOptions: true,
            // we need filterValue in return function because it can and will be updated after the component is mounted
            filterValue: filters.destination
        });
    }, [data, dispatch, filters.destination]);

    return (
        <div className="col-4">
            <label htmlFor="destination">Destinace</label>
            <CustomSelect {...searchOptions}/>
        </div>
    )
};

const PrizeFilter = () => {
    const {currency, convertToChosenCurrency, convertFromChosenCurrency} = useContext(CurrencyContext);
    const {filters, dispatch} = useContext(SearchContext);
    const {data} = useContext(FilterApiContext);
    const [searchOptions, setSearchOptions] = useState({
        id: "price",
        min: 0,
        max: 50000,
        allowCross: false,
        tipFormatter: value => `${value} ${currency.code}`
    });
    const [currentValue, setCurrentValue] = useState([searchOptions.min, searchOptions.max]);

    useEffect(() => {
        const handleAfterChange = (value) => {
            // convert price to base currency
            value = value.map((price) => {
                return convertFromChosenCurrency(price);
            });
            dispatch({type: "SET_PRIZE", prize: value})
        };

        const handleChange = (value) => {
            setCurrentValue(value);
        };

        // convert price in filters to currently selected currency
        const defaultValue = filters.prize.map((price) => {
            return convertToChosenCurrency(price);
        });
        setCurrentValue(defaultValue);
        if (filters.prize[0] === undefined || filters.prize[1] === undefined) {
            let initialValue = [convertToChosenCurrency(data.minPrice), convertToChosenCurrency(data.maxPrice)];
            console.log(data.maxPrice)
            setCurrentValue(initialValue);
            // convert price to base currency
            initialValue = initialValue.map((price) => {
                return convertFromChosenCurrency(price);
            });
            dispatch({type: "SET_PRIZE", prize: initialValue});
        }
        setSearchOptions({
            id: "price",
            min: convertToChosenCurrency(data.minPrice),
            max: convertToChosenCurrency(data.maxPrice),
            allowCross: false,
            tipFormatter: value => `${value} ${currency.code}`,
            onChange: handleChange,
            onAfterChange: handleAfterChange
        });
    }, [data, dispatch, filters.prize, currency]);

    return (
        <div className=" col-4">
            <label htmlFor="price">Cena</label>
            <Range {...searchOptions} value={currentValue}/>
            <div className="prices_filter">
                <span>{convertToChosenCurrency(filters.prize[0])} {currency.code}</span>
                <span>{convertToChosenCurrency(filters.prize[1])} {currency.code}</span>
            </div>
        </div>
    )
};

const FoodFilter = () => {
    const {dispatch, filters} = useContext(SearchContext);
    const {data} = useContext(FilterApiContext);
    const [searchOptions, setSearchOptions] = useState({
        options: [],
        inputId: "food",
        defaultOptions: true,
        filterValue: filters.food
    });

    useEffect(() => {
        const handleChange = (event) => {
            dispatch({type: "SET_FOOD", food: event.value})
        };

        const getOptions = () => {
            return data.boardings.map((e) => ({
                label: e.descriptiveName,
                value: e.name
            }));
        };

        setSearchOptions({
            options: getOptions(),
            onChange: handleChange,
            inputId: "food",
            defaultOptions: true,
            filterValue: filters.food
        });
    }, [data, dispatch, filters.food]);

    return (
        <div className="col-4">
            <label htmlFor="food">Strava</label>
            <CustomSelect {...searchOptions}/>
        </div>
    )
};

const DateRangeFilter = () => {
    const {filters, dispatch} = useContext(SearchContext);

    const handleChange = (value) => {
        dispatch({type: "SET_DATE", date: value})
    };

    return (
        <div className="col-4">
            <label htmlFor="date">Od &ndash; do</label>
            <DateRangePicker id="date"
                             value={filters.date}
                             onChange={handleChange}
                             calendarIcon={null}
                             clearIcon={null}
            />
        </div>
    )
};

const TravelTypeFilter = () => {
    const {dispatch, filters} = useContext(SearchContext);
    const {data} = useContext(FilterApiContext);
    const [searchOptions, setSearchOptions] = useState({
        options: [],
        inputId: "travelType",
        defaultOptions: true,
        filterValue: filters.travelType
    });

    useEffect(() => {

        const handleChange = (event) => {
            dispatch({type: "SET_TRAVEL_TYPE", travelType: event.value})
        };

        const getOptions = () => {
            return data.accommodationTypes.map((e) => ({
                label: e.descriptiveName,
                value: e.name
            }));
        };

        setSearchOptions({
            options: getOptions(),
            onChange: handleChange,
            inputId: "travelType",
            defaultOptions: true,
            filterValue: filters.travelType,
        });
    }, [data, dispatch, filters.travelType]);

    return (
        <div className="col-4">
            <label htmlFor="travelType">Typ pobytu</label>
            <CustomSelect {...searchOptions}/>
        </div>
    )
};

const TravelersFilter = () => {
    const {dispatch, filters} = useContext(SearchContext);
    const {data} = useContext(FilterApiContext);
    const [searchOptions, setSearchOptions] = useState({
        options: [],
        inputId: "travelers",
        defaultOptions: true,
        filterValue: filters.travelers
    });

    useEffect(() => {

        const handleChange = (event) => {
            dispatch({type: "SET_TRAVELERS", travelers: event.value})
        };

        const getOptions = () => {
            return data.rooms.map((e) => ({
                label: `${e.adults} dospělí, ${e.children} dětí`,
                value: `${e.adults} + ${e.children}`
            }));
        };

        setSearchOptions({
            options: getOptions(),
            onChange: handleChange,
            inputId: "travelers",
            defaultOptions: true,
            filterValue: filters.travelers
        });
    }, [data, dispatch, filters.travelers]);

    return (
        <div className="col-4">
            <label htmlFor="travelers">Osoby</label>
            <CustomSelect {...searchOptions}/>
        </div>
    )
};

const FilterTab = () => {
    return (
        <header className="top-bar">
            <MobileFilter/>
            <section className="filters" id="filters">
                <form action="#" method="get">
                    <div className="form-row">
                        <div className="col-lg-6 p-lg-0">
                            <div className="form-row">
                                <DestinationFilter/>
                                <PrizeFilter/>
                                <FoodFilter/>
                            </div>
                        </div>
                        <div className="col-lg-6 p-lg-0">
                            <div className="form-row">
                                <DateRangeFilter/>
                                <TravelTypeFilter/>
                                <TravelersFilter/>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </header>
    )
};


export default FilterTab;

