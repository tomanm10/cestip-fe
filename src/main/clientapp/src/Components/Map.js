import React, {useContext, useEffect, useState} from "react";
import {VectorMap} from "react-jvectormap";
import {SearchContext} from "../Contexts/SearchContext";
import {FilterApiContext} from "../Contexts/FilterApiContext";

const Map = () => {
    const {dispatch, filters} = useContext(SearchContext);
    const {data} = useContext(FilterApiContext);
    const [markers, setMarkers] = useState([]);

    const getMarkers = () => {
        return data.destinations.map((e) => ({
            latLng: [e.lat, e.lng],
            name: e.name
        }));
    };

    useEffect(() => {
        setMarkers(getMarkers());
    }, [data]);

    const initialStyle = {
        initial: {
            fill: '#F8E23B',
            stroke: '#383f47'
        }
    };

    const handleMarkerClick = (event, code) => {
        dispatch({type: "SET_DESTINATION", destination: markers[code].name});

        console.log(markers[code]);

        /// Loving it - tips stay indefinitely
        Array.from(document.getElementsByClassName("jvectormap-tip")).forEach((el) => {
            el.style.display = 'none'
        });
    };

    return (
        <section className="destinations" id="destinations">
            <h1>Destinace</h1>
            <VectorMap map={"world_mill"}
                       backgroundColor="#3b96ce"
                       zoomOnScroll={false}
                       containerStyle={{
                           width: "100%",
                           height: "30rem"
                       }}
                       markerStyle={initialStyle}
                       markers={markers}
                       onMarkerClick={handleMarkerClick}
                       createRef="map"
            />
        </section>
    );
};

export default Map;