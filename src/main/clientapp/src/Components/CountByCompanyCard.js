import React, {createRef, useContext, useEffect, useState} from "react";
import CanvasJSReact from "../lib/canvasjs.react";
import {SearchContext} from "../Contexts/SearchContext";
import useApiService from "../utils/ApiService";
import UrlManipulator from "../utils/UrlManipulator";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

const CountByCompanyCard = () => {

    const {filters} = useContext(SearchContext);
    const [data, isLoading, isError, setUrl] = useApiService("statistics/term-count?" + UrlManipulator.writeFiltersForApi(filters), []);

    const [state, setState] = useState({
        animationEnabled: true,
        data: [{
            type: "pie",
            startAngle: 75,
            toolTipContent: "<b>{label}</b>: {y}",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{label} - {y}",
            dataPoints: [{y: 100, label: "loading"}]
        }]
    });
    const chart = createRef();


    useEffect(() => {
        const getData = () => {
            return data.map((e) => {
                return {label: e.travelAgency, y: e.count}
            })
        };


        setState({
            animationEnabled: true,
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}",
                showInLegend: "true",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}",
                dataPoints: getData()
            }]
        });
    }, [data]);

    useEffect(() => {
        setUrl("statistics/term-count?" + UrlManipulator.writeFiltersForApi(filters));
    }, [filters]);

    return (
        <div className="card mb-2 flex-grow-1">
            <div className="card-header">
                <ul className="nav nav-pills card-header-pills">
                    <li className="nav-item">
                        <a className="nav-link active" data-toggle="tab" href="#countTravels" role="tab"
                           aria-selected="true">Počet
                            zájezdů</a>
                    </li>
                </ul>
            </div>
            <div className="card-body">
                <div className="tab-pane active" id="countTravels">
                    <CanvasJSChart options={state}
                                   ref={ref => chart}/>
                </div>
            </div>
        </div>
    )

}

export default CountByCompanyCard;