import React, {useEffect, useState} from 'react';
import Select from 'react-select';


const CustomSelect = (props) => {

    const [selectProps, setSelectProps] = useState(props);

    useEffect(() => {
        setSelectProps({
            search: true,
            placeholder: "Vyberte",
            className: "react-select-container",
            classNamePrefix: "react-select",
            inputId: "travelers",
            noOptionsMessage: () => "Žádné možnosti",

            ...props
        });
    }, [props]);

    const findLabelInOptions = (value) => {

        if (selectProps.options.length === 0) {
            return value;
        }

        const option = selectProps.options.find(
            e => e.value === selectProps.filterValue
        );

        return option === undefined ? value : option.label;
    };


    return <Select {...selectProps}
                   value={selectProps.filterValue === "" ? undefined : {
                       value: selectProps.filterValue,
                       label: findLabelInOptions(selectProps.filterValue)
                   }}/>;
};

export default CustomSelect;