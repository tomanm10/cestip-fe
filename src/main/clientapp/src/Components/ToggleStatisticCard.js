import React, {createRef, useContext, useEffect, useState} from "react";
import CanvasJSReact from "../lib/canvasjs.react";
import useApiService from "../utils/ApiService";
import UrlManipulator from "../utils/UrlManipulator";
import {SearchContext} from "../Contexts/SearchContext";

const CanvasJS = CanvasJSReact.CanvasJS;
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

const ToggleStatisticCard = () => {

    const {filters} = useContext(SearchContext);


    const averagePriceChart = createRef();
    const priceDevelopmentChart = createRef();

    const [evolutionData, developmentIsLoading, developmentIsError, evolutionSetUrl] = useApiService("statistics/prices/avg/evolution?" + UrlManipulator.writeFiltersForApi(filters), []);
    const [averageData, averageIsLoading, averageIsError, averageSetUrl] = useApiService("statistics/prices/avg?" + UrlManipulator.writeFiltersForApi(filters), []);


    const addSymbols = (e) => {
        const suffixes = ["", "K", "M", "B"];
        let order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
        if (order > suffixes.length - 1)
            order = suffixes.length - 1;
        const suffix = suffixes[order];
        return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
    };


    const [averageKey, setAverageKey] = useState(1);
    const [developmentKey, setDevelopmentKey] = useState(1);


    const [evolutionOptions, setEvolutionOptions] = useState({
        animationEnabled: true,
        theme: "light2",
        axisY: {
            title: "Average price",
            includeZero: false
        },
        toolTip: {
            shared: true
        },
        data: []
    });


    const [averageOptions, setAverageOptions] = useState({
        animationEnabled: true,
        theme: "light2",
        axisX: {
            title: "Travel agency",
            reversed: true,
        },
        axisY: {
            title: "Average price",
            labelFormatter: addSymbols
        },
        data: [{
            type: "bar",
            dataPoints: []
        }]
    });


    useEffect(() => {
        const getOptions = () => {
            return averageData.map((e) => {
                return {y: e.price, label: e.travelAgency}
            });
        };

        setAverageOptions({
            animationEnabled: true,
            theme: "light2",
            axisX: {
                title: "Travel agency",
                reversed: true,
            },
            axisY: {
                title: "Average price",
                labelFormatter: addSymbols
            },
            data: [{
                type: "bar",
                dataPoints: getOptions()
            }]
        });

        setAverageKey(averageKey + 1);


    }, [averageData]);

    useEffect(() => {
        const getData = () => {
            return evolutionData.map((e) => {
                    return {
                        type: "line",
                        name: e.travelAgency,
                        showInLegend: true,
                        dataPoints: e.prices.map((e) => {
                            return {y: e.price, label: new Date(e.date).toDateString()}
                        })
                    }
                }
            );
        };

        setEvolutionOptions(
            {
                animationEnabled: true,
                theme: "light2",
                axisY: {
                    title: "Average price",
                    includeZero: false
                },
                toolTip: {
                    shared: true
                },
                data: getData()
            }
        );

        setDevelopmentKey(developmentKey + 1);


    }, [evolutionData]);


    useEffect(() => {
        evolutionSetUrl("statistics/prices/avg/evolution?" + UrlManipulator.writeFiltersForApi(filters));
        averageSetUrl("statistics/prices/avg?" + UrlManipulator.writeFiltersForApi(filters));

        document.getElementById("development").classList.add("active");
        document.getElementById("average").classList.add("active");

    }, [filters]);


    useEffect(() => {
        const isActive = document.querySelector(".nav-link[href='#average']").classList.contains("active");
        if (!averageIsLoading && averageKey > 2 && !isActive) {
            document.getElementById("average").classList.remove("active");
        }
    }, [averageIsLoading]);

    useEffect(() => {
        const isActive = document.querySelector(".nav-link[href='#development']").classList.contains("active");
        if (!developmentIsLoading && developmentKey > 2 && !isActive) {
            setTimeout(() => {
                document.getElementById("development").classList.remove("active");
            }, 1)
        }
    }, [developmentIsLoading]);


    return (
        <div className="card mb-2 flex-grow-1">
            <div className="card-header d-flex align-items-center justify-content-between pd-y-5">
                <ul className="nav nav-pills card-header-pills">
                    <li className="nav-item">
                        <a className="nav-link active" data-toggle="tab" href="#average" role="tab"
                           aria-selected="true">Průměrná cena</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#development" role="tab"
                           aria-selected="false">Vývoj cen</a>
                    </li>
                </ul>
                <button type="button" className="btn-icon" data-toggle="modal" data-target="#settings">
                    <svg viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M31.8875 20.2584C31.9625 19.6959 32 19.1148 32 18.4961C32 17.8961 31.9625 17.2962 31.8687 16.7338L35.675 13.7716C36.0125 13.5091 36.1062 13.003 35.9 12.628L32.3 6.40372C32.075 5.99127 31.6062 5.86003 31.1937 5.99127L26.7125 7.79106C25.775 7.07864 24.7812 6.47871 23.675 6.02876L23 1.26681C22.925 0.816867 22.55 0.498154 22.1 0.498154H14.9C14.45 0.498154 14.0937 0.816867 14.0187 1.26681L13.3437 6.02876C12.2375 6.47871 11.225 7.09739 10.3062 7.79106L5.82496 5.99127C5.41246 5.84129 4.94371 5.99127 4.71871 6.40372L1.13746 12.628C0.912462 13.0217 0.987461 13.5091 1.36246 13.7716L5.16871 16.7338C5.07496 17.2962 4.99996 17.9149 4.99996 18.4961C4.99996 19.0773 5.03746 19.6959 5.13121 20.2584L1.32496 23.2205C0.987461 23.483 0.893712 23.9892 1.09996 24.3641L4.69996 30.5884C4.92496 31.0009 5.39371 31.1321 5.80621 31.0009L10.2875 29.2011C11.225 29.9135 12.2187 30.5134 13.325 30.9634L14 35.7253C14.0937 36.1753 14.45 36.494 14.9 36.494H22.1C22.55 36.494 22.925 36.1753 22.9812 35.7253L23.6562 30.9634C24.7625 30.5134 25.775 29.9135 26.6937 29.2011L31.175 31.0009C31.5875 31.1509 32.0562 31.0009 32.2812 30.5884L35.8812 24.3641C36.1062 23.9517 36.0125 23.483 35.6562 23.2205L31.8875 20.2584ZM18.5 25.2453C14.7875 25.2453 11.75 22.2081 11.75 18.4961C11.75 14.784 14.7875 11.7469 18.5 11.7469C22.2125 11.7469 25.25 14.784 25.25 18.4961C25.25 22.2081 22.2125 25.2453 18.5 25.2453Z"
                            fill="#4A5156"/>
                    </svg>
                </button>
            </div>
            <div className="card-body">
                <div className="tab-content">
                    <div className="tab-pane active" id="average">
                        <CanvasJSChart options={averageOptions}
                                       key={averageKey}
                                       onRef={ref => {
                                           if (averagePriceChart.current === null) {
                                               averagePriceChart.current = ref;
                                           }
                                       }}/>
                    </div>
                    <div className="tab-pane active" id="development">
                        <CanvasJSChart options={evolutionOptions}
                                       key={developmentKey}
                                       onRef={ref => {
                                           if (priceDevelopmentChart.current === null) {
                                               priceDevelopmentChart.current = ref;
                                           }
                                       }}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ToggleStatisticCard;