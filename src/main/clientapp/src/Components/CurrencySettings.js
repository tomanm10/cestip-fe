import React, {useContext} from "react";
import {CurrencyContext} from "../Contexts/CurrencyContext";


const CurrencySettings = () => {
    const {currency, setCurrency} = useContext(CurrencyContext);

    const currencyArray = [
        {
            name: "EURO",
            code: "EUR",
            value: 0.037
        },
        {
            name: "Česká koruna",
            code: "CZK",
            value: 1
        },
        {
            name: "Polský złotý",
            code: "PLN",
            value: 0.167
        }
    ];

    const handleChange = (e) => {
        setCurrency(currencyArray.find(((curr) => (curr.code === e.target.value))));
    };


    const renderOptions = () => {
        return currencyArray.map((e) => {
            return (<div className="custom-control custom-radio" key={e.code}>
                    <input type="radio" className="custom-control-input" id={e.code}
                           defaultChecked={currency.code === e.code}
                           name="currency" value={e.code}/>
                    <label className="custom-control-label" htmlFor={e.code}>
                        {e.name} ({e.code})
                    </label>
                </div>
            )
        })
    }


    return (

        <div className="modal fade" id="settings" tabIndex="-1" role="dialog" aria-labelledby="settingsTitle"
             aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title text-center" id="settingsTitle">Nastavení</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Zavřít">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form action="#" method="post" onChange={handleChange}>
                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Uvádět ceny v:</legend>
                                    <div className="col-sm-8">
                                        {renderOptions()}
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>)

};

export default CurrencySettings;
