import React from "react";

const About = () => {
    return (
        <section className="about" id="about">
            <h1>O projektu</h1>
            <p>Projekt CesTip vznikl jako semestrální práce pro předmět Řízení softwarových projektů na ČVUT FEL.</p>
            <p>Webová stránka porovnává jednotlivé cestovní kanceláře na českém trhu.</p>
            <p>Primární využití této webové stránky vidíme u cestovních kanceláří, které díky ní budou moci efektivněji
                porovnávat svoji nabídku s konkurencí. Statistické údaje se mohou hodit i samotným zákazníkům při výběru
                dovolené.</p>
            <p>Na projektu spolupracovali: Vojtěch Holub, Jan Kolovecký, Zdeněk Kotrlý, Filip Machulda, Hana Marková,
                Matěj Mužátko, Nikola Sokolová a Michal Toman.</p>
        </section>
    )
};

export default About;
