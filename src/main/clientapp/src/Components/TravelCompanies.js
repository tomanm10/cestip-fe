import React, {useState} from "react";
import CustomSelect from "./CustomSelect";
import useApiService from "../utils/ApiService";


const TravelCompanies = () => {
    const defaultData = [{
        id: 0,
        name: "",
        description: "",
        rating: 0,
        url: "",
        logoUrl: ""
    }];

    const [data, isLoading, isError] = useApiService('travelagencies', defaultData);

    // const [sort, setSort] = useState("1");
    //
    // const options = [
    //     {label: 'Průměrná cena', value: "1"},
    //     {label: 'Počet zájezdů', value: "2"},
    //     {label: 'Název', value: "3"}];
    //
    // const handleChange = (event) => {
    //     setSort(event.value);
    // };
    //
    // const searchOptions = {
    //     options: options,
    //     onChange: handleChange,
    //     className: "react-select-container travel-agencies-sort-select",
    //     inputId: "sort",
    //     filterValue: sort
    // };

    const sortAgencies = () => {
        data.sort((a, b) => {
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        })
    };

    return (
        <section className="travel-agencies" id="travel-agencies">
            <h1>Cestovní kanceláře</h1>
            {/*<div className="travel-agencies-sort">*/}
            {/*    <form className="form-inline" action="#" method="get">*/}
            {/*        <label htmlFor="sort" className="pr-2">Řadit podle</label>*/}
            {/*        <CustomSelect {...searchOptions}/>*/}
            {/*    </form>*/}
            {/*</div>*/}
            <div className="cards">
                {sortAgencies()}
                {data.map((travelCompany) => {
                    return (
                        <div className="card" key={travelCompany.id}>
                            <div className="card-body">
                                {travelCompany.name}
                            </div>
                        </div>
                    );
                })}
            </div>
        </section>
    )
};

export default TravelCompanies;